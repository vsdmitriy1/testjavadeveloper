package com.veter.service;

import com.veter.model.InitialData;
import org.springframework.web.client.RestTemplate;

public class DataService {
    public InitialData[] getInitialData() {
        String url = ("http://www.mocky.io/v2/5c51b9dd3400003252129fb5");
        return new RestTemplate().getForObject(url, InitialData[].class);
    }
}
