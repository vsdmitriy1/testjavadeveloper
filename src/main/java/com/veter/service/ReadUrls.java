package com.veter.service;

import com.veter.model.FinalData;
import com.veter.model.InitialData;
import com.veter.model.SourceData;
import org.springframework.web.client.RestTemplate;

public class ReadUrls implements Runnable {
    InitialData initialD;
    FinalData result;
    RestTemplate restTemplate;

    public ReadUrls(InitialData initialD, RestTemplate restTemplate) {
        this.initialD = initialD;
        this.restTemplate = restTemplate;
    }

    @Override
    public void run() {
        String url = (initialD.sourceDataUrl);
        SourceData sd = restTemplate.getForObject(url, SourceData.class);

        url = (initialD.tokenDataUrl);
        FinalData fd = restTemplate.getForObject(url, FinalData.class);

        fd.id = initialD.id;
        fd.urlType = sd.urlType;
        fd.videoUrl = sd.videoUrl;

        result = fd;
    }

    public FinalData getResult() {
        return result;
    }
}