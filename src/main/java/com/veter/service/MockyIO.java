package com.veter.service;

import com.veter.model.FinalData;
import com.veter.model.InitialData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;


@RequiredArgsConstructor()
@Service
public class MockyIO {
    private final RestTemplate restTemplate;

    public List<FinalData> run() {

        DataService dataService = new DataService();
        InitialData[] initialDataList = dataService.getInitialData();

        ReadUrls[] s = new ReadUrls[initialDataList.length];
        for (int i = 0; i < initialDataList.length; i++) {
            s[i] = new ReadUrls(initialDataList[i], restTemplate);
        }
        Thread[] threads = new Thread[s.length];
        for (int i = 0; i < s.length; i++) {
            Thread thread = new Thread(s[i]);
            thread.start();
            threads[i] = thread;
        }
        try {
            for (Thread thread : threads) {
                thread.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<FinalData> listFD = new ArrayList<>();
        for (ReadUrls ru : s) {
            listFD.add(ru.getResult());
        }
        return listFD;
        /*for (ReadUrls ru : s) {
            System.out.println(ru.getResult());
        }*/
    }

}
