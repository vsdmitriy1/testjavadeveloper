package com.veter.model;

public class InitialData {
    public int id;
    public String sourceDataUrl;
    public String tokenDataUrl;

    public InitialData() {
    }

    public InitialData(int id, String sourceDataUrl, String tokenDataUrl) {
        this.id = id;
        this.sourceDataUrl = sourceDataUrl;
        this.tokenDataUrl = tokenDataUrl;
    }
}
