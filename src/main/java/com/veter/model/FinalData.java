package com.veter.model;

public class FinalData {
    public int id;
    public UrlType urlType;
    public String videoUrl;
    public String value;
    public int ttl;

    public FinalData() {
    }

    public FinalData(String value, int ttl) {
        this.value = value;
        this.ttl = ttl;
    }

    @Override
    public String toString() {
        return "FinalData{" +
                "id=" + id +
                ", urlType=" + urlType +
                ", videoUrl='" + videoUrl + '\'' +
                ", value='" + value + '\'' +
                ", ttl=" + ttl +
                '}';
    }
}
