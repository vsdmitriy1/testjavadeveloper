package com.veter.model;

public class SourceData {
    public UrlType urlType;
    public String videoUrl;

    public SourceData(UrlType urlType, String videoUrl) {
        this.urlType = urlType;
        this.videoUrl = videoUrl;
    }

    public SourceData() {
    }

    @Override
    public String toString() {
        return "FinalData{" +
                ", urlType=" + urlType +
                ", videoUrl='" + videoUrl + '\'' +
                '}';
    }
}
