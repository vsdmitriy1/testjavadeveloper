package com.veter.model;

public enum UrlType {
    LIVE,
    ARCHIVE
}
