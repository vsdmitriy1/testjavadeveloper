package com.veter.controller;

import com.veter.model.FinalData;
import com.veter.service.MockyIO;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Controller for receiving and aggregating data from several services.
 *
 * @author dprigozhaev on 01.02.2020
 */

@RequiredArgsConstructor()
@RestController
public class DataController {

    private final MockyIO mockyIO;


    @GetMapping("/finalData")
    public List<FinalData> run() {
        return mockyIO.run();
    }

}