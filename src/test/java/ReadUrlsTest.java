
import junit.framework.TestCase;
import com.veter.model.FinalData;
import com.veter.model.InitialData;
import com.veter.model.SourceData;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;
import com.veter.service.ReadUrls;

import static com.veter.model.UrlType.LIVE;

@RunWith(MockitoJUnitRunner.class)
public class ReadUrlsTest extends TestCase {

    public void setUp() throws Exception {
        super.setUp();
    }

    @Mock
    private RestTemplate restTemplate;

    @Test
    public void testTestRun() {
        String sourceDataUrl = "http://www.mocky.io/v2/5c51b230340000094f129f5d";
        String tokenDataUrl = "https://www.mocky.io/v2/5c51b5b6340000554e129f7b?mocky-delay=1s";

        SourceData sourceData = new SourceData(LIVE, "rtsp://127.0.0.1/1");
        FinalData finalData = new FinalData("fa4b588e-249b-11e9-ab14-d663bd873d93", 120);

        InitialData initialData = new InitialData(1, sourceDataUrl, tokenDataUrl);

        finalData.id = initialData.id;
        finalData.urlType = sourceData.urlType;
        finalData.videoUrl = sourceData.videoUrl;

        Mockito.when(restTemplate.getForObject(sourceDataUrl, SourceData.class)).thenReturn(sourceData);
        Mockito.when(restTemplate.getForObject(tokenDataUrl, FinalData.class)).thenReturn(finalData);

        ReadUrls result = new ReadUrls(initialData, restTemplate);
        result.run();

        Assert.assertNotNull(result);
        Assert.assertEquals(finalData.id, result.getResult().id);
        Assert.assertEquals(finalData.urlType, result.getResult().urlType);
        Assert.assertEquals(finalData.ttl, result.getResult().ttl);
        Assert.assertEquals(finalData.videoUrl.compareTo(result.getResult().videoUrl), 0);
        Assert.assertEquals(finalData.value.compareTo(result.getResult().value), 0);

    }

}